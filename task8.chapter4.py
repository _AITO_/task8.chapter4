class MoneyFmt():
    def __init__(self, number):
        self.number = number

    def calc(self):
        num = round(self.number, 2)
        n = '{0:,}'.format(num)
        result = '${}'.format(n)
        return result

    def update(self, new_numb):
        self.number = new_numb
        return self.number

    def str(self):
        return self.calc()
a = MoneyFmt(5154618455466)
print(a.calc())
a.update(53752.4593)
print(a)